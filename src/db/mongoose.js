const mongoose = require('mongoose');

mongoose.connect('mongodb://admindb:adminpass@localhost:27017/nodeauth',{
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
}).then(() =>{
  console.log('connected to database');
}).catch(() =>{
  console.log('failed connected to database');
});