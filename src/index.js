const express = require('express')
require('./db/mongoose')
const userRoutes = require('./router/user')
const postRoutes = require('./router/post')

const app = express();
const port =  process.env.PORT || 3005

app.use(express.json())

app.use(userRoutes)
app.use(postRoutes)

app.listen(port,() =>{
    console.log('server is up on ' + port);
})