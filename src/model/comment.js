const mongoose = require('mongoose')

const CommentSchema = new mongoose.Schema({
  comment:{
    type: String,
    trim: true
  },
  author:{
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Post'
  },
  createAt:{
    type: Date,
    default: Date.now
  }
});

const Comment = mongoose.model('Comment', CommentSchema)

module.exports = Comment;
